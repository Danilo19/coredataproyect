//
//  ViewController.swift
//  CoreDataProyect
//
//  Created by Danilo Angamarca on 24/1/18.
//  Copyright © 2018 Danilo Angamarca. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    
    @IBOutlet weak var NameTextField: UITextField!
    @IBOutlet weak var AddressTextField: UITextField!
    @IBOutlet weak var PhoneTextField: UITextField!
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func savePerson() {
        let entityDescription = NSEntityDescription.entity(forEntityName: "Persona", in: manageObjectContext)
        let persona = Persona(entity: entityDescription!, insertInto: manageObjectContext)
        
        persona.address = AddressTextField.text ?? ""
        persona.name = NameTextField.text ?? ""
        persona.phone = PhoneTextField.text ?? ""
        
        do{
            
            try manageObjectContext.save()
            clearFields()
            
        } catch {
            print("Error")
        }
        
    }
    
    func clearFields(){
        AddressTextField.text = ""
        NameTextField.text = ""
        PhoneTextField.text = ""
        
    }
    
    func findAll(){
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Persona", in: manageObjectContext)
        
        let request:NSFetchRequest<Persona> = Persona.fetchRequest()
        
        do{
            
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
           
            for result in results {
                //print(result)
                let person = results as! Persona
                
                print("Name: \(person.name ?? "") ", terminator: "")
                print("Address: \(person.address ?? "") ", terminator: "")
                print("Phone: \(person.phone ?? "") ", terminator: "")
                print()
                print()
            }
            
            
        } catch {
            print("Error finding people")
        }
        
    }
    
    func findOne(){
         let entityDescription = NSEntityDescription.entity(forEntityName: "Persona", in: manageObjectContext)
        
        let request:NSFetchRequest<Persona> = Persona.fetchRequest()
        
        request.entity = entityDescription
        let predicate = NSPredicate(format: "name = %@", NameTextField.text!)
        request.predicate = predicate
        
        do{
            
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            if results.count > 0 {
                let match = results[0] as! Persona
                
                AddressTextField.text = match.address
                NameTextField.text = match.name
                PhoneTextField.text = match.phone
            }else{
                
                AddressTextField.text = "n/a"
                NameTextField.text = "n/a"
                PhoneTextField.text = "n/a"
            }
            
            
        } catch {
            print("Error finding one")
        }
    }


    @IBAction func saveButtonPressed(_ sender: Any) {
        savePerson()
    }
    
    @IBAction func findButtonPressed(_ sender: Any) {
        if(NameTextField.text == ""){
        findAll()
        }else{
        findOne()
        }
    }
   
    
}

